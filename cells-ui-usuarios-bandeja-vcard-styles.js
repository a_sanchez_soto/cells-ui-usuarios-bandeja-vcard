import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-usuarios-bandeja-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.panel-top-usuario {
  width: 100%;
  background-color: #f4f4f4;
  border: 1px solid #BDBDBD;
  padding: 10px;
  float: left; }

.input {
  float: right; }

.button {
  float: left; }

.content-table-usuario {
  width: 100wh;
  padding-top: 5px;
  overflow-x: auto;
  overflow-y: hidden; }

.btn-remove {
  background-color: #CB353A !important; }

.actions-buttons-usuario {
  display: inline-flex; }

.actions-buttons-usuario button {
  margin: 0 4px; }

.btn-action-usuario {
  background-color: #1e88e5;
  border: none;
  color: white;
  padding: 11px 14px;
  text-align: center;
  font-size: 17px;
  cursor: pointer; }

.btn-refresh {
  background-color: #1464A5;
  border: none;
  color: white;
  padding: 12px 14px;
  text-align: center;
  height: 48px;
  width: 50px;
  font-size: 17px;
  cursor: pointer;
  margin-left: 5px;
  float: right; }

.input {
  height: 48px;
  color: #666;
  width: 248px;
  background-color: #fff;
  border-bottom: 1px #333 solid;
  padding-left: 10px;
  padding-top: 5px;
  font-size: 1rem;
  border-top: 1px solid #fff;
  border-left: 1px solid #fff;
  border-right: 1px solid #fff; }

.disabled-input {
  opacity: 0.5; }

@media only screen and (max-width: 420px) {
  cells-ui-autocomplete-vcard {
    width: 100%; }
  .panel-top {
    display: block; }
  .input {
    width: 100%;
    margin-bottom: 5px; }
  .btn-refresh {
    width: 100%;
    margin: 0px 0px 5px 0px; } }
`;