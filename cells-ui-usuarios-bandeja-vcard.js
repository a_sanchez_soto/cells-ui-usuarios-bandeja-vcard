import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-usuarios-bandeja-vcard-styles.js';
import '@bbva-web-components/bbva-button-default';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-theme-vcard';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
/**
This component ...

Example:

```html
<cells-ui-usuarios-bandeja-vcard></cells-ui-usuarios-bandeja-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiUsuariosBandejaVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-usuarios-bandeja-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      items: {
        type: Array
      },
      itemsDisplay: {
        type: Array
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.items = [];
    this.itemsDisplay = [];
    this.updateComplete.then(() => {
      this.getById('input-search').addEventListener('input', () => {
        this.filterItems();
        this.requestUpdate();
      });
    });
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-usuarios-bandeja-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  async setItems(data) {
    this.items = data;
    this.itemsDisplay = data;
    await this.requestUpdate();
  }

  refresh() {
    this.dispatch(this.events.refreshBandejaUsuario, {});
    this.getById('input-search').value = '';
  }

  onEdit(event) {
    this.dispatch(this.events.openEdiUsuario, event);
  }

  onRemove(event) {
    this.dispatch(this.events.removeUsuario, event);
  }

  nuevoUsuario() {
    this.dispatch(this.events.newUsuario, event);
  }

  filterItems(onlyOficina) {
    let query = this.getById('input-search').value;
    if (query) {
      query = query.toUpperCase();
      this.itemsDisplay = this.items.filter((item) => {
        let concat = `${item.registro}${item.nombreCompleto}${this.extract(item, 'oficina.nombre', '')}${this.extract(item, 'perfil.nombre', '')}${this.extract(item, 'estado.nombre', '')}${item.email}`;
        return (concat.toUpperCase().indexOf(query) >= 0);
      });
    } else {
      this.itemsDisplay = this.items;
    }
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "panel-top-usuario" >
      
      <button class = "button" @click = ${this.nuevoUsuario} >
                  <cells-icon 
                    size = "20" 
                    icon="coronita:newclient"
                    ></cells-icon> Nuevo usuario
                </button>

      <button @click = "${() => this.refresh()}" title = "Refrescar" class = "btn-refresh"><cells-icon size = "18" icon = "coronita:navigation" ></<cells-icon></button>
      <input id = "input-search" class = "input" placeholder = "Buscar" />

      </div>

      <div class = "content-table-usuario" style = "float:left; width:100%">
      <table class = "green">
        <thead> 
          <tr>
            <th>Registro</th>
            <th>Nombre completo</th>
            <th>Perfil</th>
            <th>Email</th>
            <th>Oficina</th>
            <th>Estado</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
        ${this.itemsDisplay.map((item, index) => html`
          <tr>
              <td>${item.registro}</td>
              <td>${item.nombreCompleto}</td>
              <td>${this.extract(item, 'perfil.nombre', '')}</td>
              <td>${this.extract(item, 'email', '')}</td>
              <td>${this.extract(item, 'oficina.nombre', '')}</td>
              <td>${this.extract(item, 'estado.nombre', '')}</td>
              <td>
                <div class = "actions-buttons-usuario" >
                  <button @click = "${() => this.onEdit(item, index)}" title = "Editar" class = "btn-action-usuario"><cells-icon size = "18" icon = "coronita:edit" ></<cells-icon></button>
                  <button @click = "${() => this.onRemove(item, index)}" title = "Eliminar" class = "btn-action-usuario btn-remove"><cells-icon size = "18" icon = "coronita:trash" ></<cells-icon></button>
                </div>
              </td>
            </tr>
        `)}
          
       
        </tbody>
      </table>
      </div>
      ${this.itemsDisplay.length === 0 ? html` <div style = "width: 100%;
                                                        float:left;
                                                        padding: 15px;
                                                        border: 1px solid #BDBDBD;
                                                        text-align: center;
                                                        color: #666;">Sin resultados que mostrar.</div>` : html``}

    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiUsuariosBandejaVcard.is, CellsUiUsuariosBandejaVcard);
